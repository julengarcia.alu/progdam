/* 3. Realiza un programa en Java que haga lo siguiente (solución preferentemente modular, todo en una misma clase que incluya main y el resto de funciones):

    - genere 100 números enteros aleatorios, entre 1 y 100, y los cargue en un array de 100 enteros
    - muestre el array por pantalla
    - pida al usuario un valor (entre 1 y 100 también) e indique si, o no, se encuentra en el array (búsqueda lineal en vector desordenado)
    - ordene el array con el método de la burbuja
    - vuelva a mostrar el array
    - pida otro valor, entre 1 y 100, al usuario
    - realice otra búsqueda (ahora dicotómica) indicando, igualmente, si se encuentra, o no, en el array
    - con el mismo valor de búsqueda, finalmente realizará una búsqueda lineal indicando cuántas veces ha aparecido el valor en el array */

import java.util.Scanner;

public class ej3
{
	private static final int TAM = 100;	// tamaño del array
	private final static int TOP = 100;	// valor máximo a generar
	private static int nums[] = new int[TAM];
	
	public static void main(String[] args) {
		Scanner ent = new Scanner(System.in);
		CienAleatorios();
		muestraArray();
		System.out.println("¿Qué valor quieres buscar?");
		int valor = ent.nextInt();
		if (busquedaLinealDesordenada(valor))
			System.out.println("Ese valor SI se encuentra en el array");
		else	
			System.out.println("Ese valor NO se encuentra en el array");
		ordenaBurbuja();
		muestraArray();
		System.out.println("¿Qué valor quieres buscar ahora (entre 1 y 100)?");
		valor = ent.nextInt();
		if (busquedaDicotomica(valor))
			System.out.println("Ese valor SI se encuentra en el array");
		else	
			System.out.println("Ese valor NO se encuentra en el array");
		System.out.println("El valor ha aparecido " + cuentaAparicionesOrdenada(valor) + " veces.");
		
		
		System.exit(0);
	}
	// método que carga el array con valores aleatorios
	public static void CienAleatorios()
	{
		for (int i=0 ; i < TAM ; i++)
			nums[i]	= 1 + (int)(TOP*Math.random());
	}
	
	// método que muestra el array
	public static void muestraArray()
	{
		for (int i=0; i < TAM ; i++)
			System.out.print(nums[i] + "\t");
	}
			 
	// método booleano para búsqueda lineal desordenada
	public static boolean busquedaLinealDesordenada(int valor)
	{
		for (int i=0; i < TAM ; i++)
			if (valor == nums[i])
				return true;
		return false;
	}
	
	public static void ordenaBurbuja()
	{
		boolean desordenado = true; int aux;
		for ( int tope = TAM - 2 ; (tope >= 0) && (desordenado) ; tope--)
		{
			desordenado = false;
			for (int i=0 ; i <= tope ; i++)
				if (nums[i] > nums[i+1])
				{
					desordenado = true;
					aux = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = aux;
				}
		}
	}
	
	public static boolean busquedaDicotomica(int valor)
	{
		int izq=0,der=TAM-1,centro = (izq + der)/2;
		
		while ( (izq <= der) && (nums[centro] != valor))
		{
			if (nums[centro] > valor)
				der = centro -1;
			else
				izq = centro + 1;
			centro = (izq + der)/2;
		}
		if (nums[centro] == valor)
			return true;
		else
			return false;
	}
	
	public static int cuentaAparicionesOrdenada(int valor)
	{
		int cont=0;
		
		for (int i=0 ; i < TAM ; i++)
		{
			if (nums[i] > valor)
				return cont;
			if (nums[i] == valor)
				cont++;
		}
		return cont;
	}					
}
