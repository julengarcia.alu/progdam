import java.util.*;
import java.io.*;

class Partida
{
	/* El tamaño de la matriz */
	private int tam=10;	
	
	private boolean mio[][] = new boolean[tam][tam];	// el tablero de este jugador
	private char rival[][] = new char[tam][tam];	// el tablero de su rival
	
	/** Constructor con un parámetro
	
	@param t	tamaño de la matriz (nº de filas y de columnas) */
	public Partida(int t)
	{
		tam=t;
		// PONE AMBOS TABLEROS EN BLANCO
		for(int i=0; i<tam;i++)
			for(int j=0; j<tam;j++)
			{
				mio[i][j]=false;
				rival[i][j]='-';
			}
	}
	
	public void poneBarcos()
	{
		// i y j serán las coordenadas (fila y columna, respectivamente) de cada casilla generada al azar
		int i,j;
		
		// pongo los 4 barcos de una casilla
		for (int cont=0;cont<4;/* no incremento aquí el contador */)
		{
			i=(int)(10*Math.random());
			j=(int)(10*Math.random());
			if (mio[i][j]==false)
			{
				mio[i][j]=true;
				cont++;
			}
			// Si era una casilla ocupada no incremento el contador del bucle con lo que no se contabiliza la tirada
		}
		// pongo los 3 barcos de 2 casillas
		for (int cont=0;cont<3; )
		{
			i=(int)(10*Math.random());
			j=(int)(10*Math.random());
			if (j!=tam-1)
				if (mio[i][j]==false && mio[i][j+1]==false)
				{
					mio[i][j]=mio[i][j+1]=true;
					cont++;
				}
		}
		// pongo los 2 barcos de 3 casillas
		for (int cont=0;cont<2; )
		{
			i=(int)(10*Math.random());
			j=(int)(10*Math.random());
			if (j<tam-2)
				if (mio[i][j]==false && mio[i][j+1]==false && mio[i][j+2]==false)
				{
					mio[i][j]=mio[i][j+1]=mio[i][j+2]=true;
					cont++;
				}
		}
		// pongo el barco de una casilla
		i=(int)(10*Math.random());
		j=(int)(10*Math.random());
		while ((j>=tam-3) || (mio[i][j]==true) && (mio[i][j+1]==true) && (mio[i][j+2]==true) && (mio[i][j]==true))
		{
			i=(int)(10*Math.random());
			j=(int)(10*Math.random());
		}
		mio[i][j]=mio[i][j+1]=mio[i][j+2]=mio[i][j+3]=true;
	}
				
	public void muestra()
	{
		System.out.println("MI TABLERO\t\t\t\t\tTABLERO RIVAL");
		System.out.println("==========\t\t\t\t\t=============");
		
		System.out.println("  1 2 3 4 5 6 7 8 9 10\t\t\t\t  1 2 3 4 5 6 7 8 9 10");		
		for(int i=0;i<tam;i++)
		{
			System.out.print((char)('A'+i));
			System.out.print(" ");
			for(int j=0;j<tam;j++)
			{
				if (mio[i][j]==true)
					System.out.print("o ");
				else
					System.out.print("- ");
			}
			System.out.print("\t\t\t\t");
			System.out.print((char)('A'+i));
			System.out.print(" ");
				
			
			for(int j=0;j<tam;j++)
				System.out.print(rival[i][j] + " ");
			System.out.println("");	
		}		
	}
	
	public boolean tiradaDelRival(String jugada)
	{
		int i,j;
		
		i=jugada.toLowerCase().charAt(0)-'a';
		j=jugada.toLowerCase().charAt(1)-'1';
		//System.out.println("Mi rival va a disparar en fila "+i+" y columna "+j);
		if (mio[i][j]==true)
		{
			mio[i][j]=false;
			return true;
		}
		return false;
	}
	
	public void anotoMiTirada(String jugada,char resultado)
	{
		int i,j;
		
		i=jugada.toLowerCase().charAt(0)-'a';
		j=jugada.toLowerCase().charAt(1)-'1';
		System.out.println(i +"," + j);
			
		
		rival[i][j]=resultado;
	}
}

public class ej4
{
	public static void main(String args[]) 
	{
		final int TAM = 10;
		Partida p=new Partida(TAM);
		Scanner ent=new Scanner(System.in);
		String jugada=""; char resultado;
		
/*		try
		{*/
			//System.out.println("Mi partida inicial es (o -> hay barco, - -> agua):");	
			//Runtime.getRuntime().exec("/bin/bash -c clear").waitFor();	
			p.muestra();
			p.poneBarcos();
			System.out.println("Los tableros después de colocados los barcos quedan (o -> hay barco, - -> agua):");
			//System.out.print("\f");
			p.muestra();
			// Comienzan los turnos de tiradas
			while (true)
			{
				// recibo tirada
				System.out.println("DISPARO RIVAL: Introduce fila (de A a " + (char)('A' + TAM-1) + " y columna (de 1 a " + TAM +"), por ejemplo, F7:");
				jugada=ent.nextLine();
				// mientras tenga éxito la tirada el rival seguirá disparando
				while (p.tiradaDelRival(jugada))
				{
					System.out.println("¡ ACIERTO !");
					System.out.println("DISPARO RIVAL: Introduce fila (de A a " + (char)('A' + TAM-1) + " y columna (de 1 a " + TAM +"), por ejemplo, F7:");
					jugada=ent.nextLine();
				}
				System.out.println("¡ AGUA !");
				p.muestra();
				// si la tirada tiene éxito, cambio T por A en el Partida (mi oponente ganará cuando todo el Partida esté a A)
				System.out.println("ES TU TURNO. Introduce fila (de A a " + (char)('A' + TAM-1) + " y columna (de 1 a " + TAM +"), por ejemplo, F7:");
				jugada=ent.nextLine();
				System.out.println("¿Cuál ha sido el resultado (a -> agua, t-> tocado o h-> hundido)?");
				resultado=ent.nextLine().charAt(0);
				p.anotoMiTirada(jugada,resultado);
				p.muestra();
				while (resultado != 'a')
				{
					System.out.println("ES TU TURNO. Introduce fila (de A a " + (char)('A' + TAM-1) + " y columna (de 1 a " + TAM +"), por ejemplo, F7:");
					jugada=ent.nextLine();
					System.out.println("¿Cuál ha sido el resultado (a -> agua, t-> tocado o h-> hundido)?");
					resultado=ent.nextLine().charAt(0);
					p.anotoMiTirada(jugada,resultado);
					p.muestra();
				}
			} 
//		}
/*		catch (IOException e)
		{
			e.getMessage();
		}
		catch (InterruptedException e)
		{
			e.getMessage();
		}*/
	}
}	
