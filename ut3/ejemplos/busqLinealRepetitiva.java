// Ejemplo de búsqueda lineal ENTERA y REPETITIVA(retorna las posiciones donde lo encuentra)

import java.util.Scanner;

public class busqLinealRepetitiva
{
	public static void main(String args[])
	{
		final int TAM = 5; int i;
		Scanner ent = new Scanner(System.in);
		// declaro el array de 5 valores numéricos double
		double nums[] = new double[TAM];
		// leo valores desde teclado
		for (i = 0 ; i < TAM ; i++)
			nums[i] = ent.nextDouble();
		// muestro el contenido del array en pantalla
		for (i = 0 ; i < TAM ; i++)
			System.out.print(nums[i] + "\t");
		System.out.println("¿Qué valor quieres buscar en el array?");
		double n = ent.nextDouble();
		int r[] = busquedaLinealRepetitiva(n,nums);
		for (i = 0 ; r[i] != -1 ; i++)
			System.out.println("Encontrado  en la posición " + r[i]);
	}
	
	public static int[] busquedaLinealRepetitiva(double vb, double array[])
	{
		int i,cont=0;
		int reps[] = new int[array.length + 1];
		
		for(i = 0 ; i < array.length ; i++)
			if (array[i] == vb)
			{
				reps[cont] = i + 1;
				cont++;
			}
				
		/* o también:
			for (int i = 0 ; i < array.length ; i++)
				if (array[i] == vb) return true; */
		//if (cont < array.length)
		reps[cont] = -1;
		return reps;
	}
}

