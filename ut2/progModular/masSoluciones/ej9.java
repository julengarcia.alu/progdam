/* 9. Realizar un programa que escriba en pantalla n líneas del triángulo de PASCAL utilizando un subprograma recursivo.
	En el triángulo de PASCAL los números de los lados valen 1 y los del centro se calculan sumando los dos que tiene inmediatamente encima:

					1
				1		1
			1		2		1
		1		3		3		1
	1		4		6		4		1
..............................................................................

	Se aconseja utilizar una función recursiva que calcule el valor de cada término dada su posición (fila y columna). Por ejemplo, que calcule que el elemento de la fila 5 y columna 3 es un 6. */

import java.util.Scanner;

public class ej9
{
	/* Función RECURSIVA que obtiene el valor para el triángulo de
	Pascal a partir de los valores de fila (f) y columna (c) */
	public static int valorTPascal(int f, int c)
	{
		if ((c==1) || (c==f))	// si está en algún extremo
			return 1;
		else	// algún valor interno al triángulo
			return valorTPascal(f-1,c-1) + valorTPascal(f-1,c);
	}
	
	public static void main(String[] args)
	{
		int n,f,c;
		
		Scanner ent = new Scanner(System.in);
		
		System.out.println("¿Cuántas líneas quieres ver?");
		n = ent.nextInt();
		for (f=1 ; f <= n; f++)	// varía la fila
		{
			for(c=1; c <= f ; c++)	// varía el número de columna
				System.out.print(valorTPascal(f,c));
			System.out.println("");
		}

		System.exit(0);
	}
}
