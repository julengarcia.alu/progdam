//Uso de la clase 'Disc_v2'.

public class UseDisc_v2{
	
	public static void main(String[] args){


		Disc_v2 d1, d2, d3, d4, d5;
		Musician m1, m2, m3, m4, m5;
		String title, artstName, artstIns;
		double price;

		System.out.println("\nA partir de los datos introducidos desde teclado, el programa mostrará el catálogo de discos creado.\n");


		/* Instanciación de clases */

		//Instancia predeterminada.
		m1 = new Musician();
		d1 = new Disc_v2();
		System.out.println("\nDisco 1º registrado de acuerdo con la información predeterminada por el programa.");

		/*
		  Instancias creadas inicialmente a partir del constructor por defecto, para modificar sus atributos
		  más adelante, de acuerdo con los datos facilitados por el usuario.
		*/

		m2 = new Musician();
		d2 = new Disc_v2();
		
		m3 = new Musician();
		d3 = new Disc_v2();
		
		m4 = new Musician();
		d4 = new Disc_v2();


		//Edición de las instancias 'd2' a 'd4'.
		for(int i=2; i<=4; i++){

			System.out.printf("\nTítulo del disco %dº: ",i);
			title = System.console().readLine();
			System.out.print("Nombre del artista: ");
			artstName = System.console().readLine();
			System.out.print("Instrumento del artista: ");
			artstIns = System.console().readLine();

			switch(i){
				case 2:
					d2.setTitle(title);
					m2.setName(artstName);
					m2.setInstrument(artstIns);
					d2.setMscn(m2);
					break;
				case 3:
					d3.setTitle(title);
					m3.setName(artstName);
					m3.setInstrument(artstIns);
					d3.setMscn(m3);
					break;
				case 4:
					d4.setTitle(title);
					m4.setName(artstName);
					m4.setInstrument(artstIns);
					d4.setMscn(m4);
			}
		}

		/*
		  Llamada al método estático 'selectDiscRandomly()' para seleccionar aletaoriamente el disco al que
		  le será aplicado el descuento, cuyo atributo 'hasDiscount', entonces, tomará el valor de 'true'.
		*/
		switch(Disc_v2.slctDiscRandomly()){

			case 1: d1.setHasDiscount(true); break;
			case 2: d2.setHasDiscount(true); break;
			case 3: d3.setHasDiscount(true); break;
			case 4: d4.setHasDiscount(true);
		}


		//Instancia duplicada a partir del constructor de copia.
		d5 = new Disc_v2(d2);
		System.out.println("\nDisco 5º registrado como copia del segundo.");


		//Salida en pantalla de los datos de los objetos creados.
		System.out.println("\n\nCATÁLOGO DE DISCOS");
		Disc_v2.getDiscInfo(d1);
		Disc_v2.getDiscInfo(d2);
		Disc_v2.getDiscInfo(d3);
		Disc_v2.getDiscInfo(d4);
		Disc_v2.getDiscInfo(d5);


		System.out.println("\n");
	}
}
