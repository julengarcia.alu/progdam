/* 3. Crea un programa de gestión de discos de música. Para ello, define una clase Disco, sabiendo que para cada disco queremos registrar el título (String), grupo o artista (String), precio (por defecto, 15 euros pero algún disco puede estar en oferta, rebajándolo un 20%). Añade a la clase los constructores y métodos que consideres útiles o necesarios.

 El programa debe crear 3 discos, con valores de títulos y grupos introducidos desde teclado por el usuario. Además el programa pondrá en oferta un único disco, elegido al azar con Math.random(). Finalmente, mostrará todos los discos con sus precios. */
 
 class disco
 {
 	private String titulo;
 	private String grupo;
 	private double precio=15;
 	// métodos: constructores, setters, getters y otros
 	public disco() { titulo = "Street Legal"; grupo = "Bob Dylan"; }
 	public disco(String t, String g) { titulo = t; grupo = g; }
 	public void setTitulo(String t) { titulo = t; }
 	public void setGrupo(String g) { grupo = g; }
 	public String getTitulo() { return titulo; }
 	public String getGrupo() { return grupo; }
 	public void muestraDisco()
 	{
 		System.out.println("Título: " + titulo + ", grupo: " + grupo + ", precio: " + precio);
 	}
 	public void oferta()
 	{
 		precio *= 0.8;		// precio = precio*0.8;
 	}
 		
 }
 
 public class ej3
 {
 	public static void main(String[] args) {
 		// creamos 3 discos
 		disco d1 = new disco();
 		disco d2 = new disco("The dark side of the moon","Pink Floyd");
 		disco d3 = new disco("Remain in light","Talking Heads");
 		// genero un entero al azar entre 1 y 3
 		int n = (int)(1 + 3*Math.random());
 		switch(n)
 		{
 			case 1: d1.oferta();break;
 			case 2: d2.oferta();break;
 			case 3: d3.oferta();
 		}
 		d1.muestraDisco();d2.muestraDisco();d3.muestraDisco();
 		// uno al azar lo ponemos en oferta
 		
 		System.exit(0);
 	}
 }
