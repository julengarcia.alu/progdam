import java.io.*;
public class ej1
{
  public static void main(String[] args) throws IOException
  {
    String s = new String("En un lugar de la mancha de cuyo nombre no quiero acordarme, ");
    s = s + "no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero, ";
    s = s + "adarga antigua, rocín flaco y galgo corredor...";
    // De los arrays en Java hablaremos en el siguiente capítulo, pero antes aquí tenemos un ejemplo...
    char[] arr = new char[s.length()];
    int car = 0;  
    // el flujo de entrada será el String ...            
    StringReader flujoInput = new StringReader(s);
    // ... y el flujo de salida el array
    CharArrayWriter flujoOutput = new CharArrayWriter();
/*    try
    {    */
         while ((car = flujoInput.read()) != -1)
         	// se va escribiendo cada caracter al flujo de salida
           flujoOutput.write(car);
         // cuando ya esta todo se envía al array
         arr = flujoOutput.toCharArray();
         System.out.println(arr);
 /*   }
    catch (IOException e) { 
         e.printStackTrace(); 
    }
    finally
    { */
      flujoInput.close();
      flujoOutput.close();
   // }
  }
}
