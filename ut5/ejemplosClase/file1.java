// Ejemplo inicial de la clase File

import java.io.File;
import java.util.Date;

public class file1 {
	public static void main(String[] args)
	{
			File file = new File("image/gato.jpg");
			if ( file.exists() )
			{
				System.out.println("tiene un tamaño de " + file.length() + " bytes");
				System.out.println("Puede ser leido? " + file.canRead());
				System.out.println("Puede ser escrito? " + file.canWrite());
				System.out.println("Puede ser ejecutado? " + file.canExecute());
				System.out.println("Es un directorio? " + file.isDirectory());
				System.out.println("Es un archivo? " + file.isFile());
				System.out.println("Es absoluto? " + file.isAbsolute());
				System.out.println("está oculto? " + file.isHidden());
				System.out.println("La ruta absoluta es " + file.getAbsolutePath());
				System.out.println("Ultima modificación: "+new Date(file.lastModified()));
			}
			else
				System.out.println("El fichero no existe");
	}
}
