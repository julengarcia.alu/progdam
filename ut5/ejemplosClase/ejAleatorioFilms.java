import java.io.*;
import java.util.Scanner;

class Film
{
	private static final int maxString=100;
	// 210 Bytes es el resultado de la siguiente operación
	private static final int TAM_FILM = 2*(maxString+1)+2*Integer.SIZE/8;

	private int cod;
	private String title;
	private int year;
	private String director;

	public Film(int c, String t, int y, String d)
	{
		cod=c;year=y;
		if (t.length() > maxString)
			title=t.substring(0,maxString);
		else
			title=t;
		if (d.length() > maxString)
			director=d.substring(0,maxString);
		else
			director=d;
	}

	public void show()
	{
		if (cod != 0)
			System.out.printf("------\n%d\t%s\n\t%d\tDirector: %s\n-----\n",cod,title,year,director);
		else
			System.out.print("Esa película no existe\n");
	}
	// escribe una película al fichero RAF
	public void writeFile(RandomAccessFile raf) 
	{
		try
		{
			// sitúo la marca de posición donde corresponda según el código de la pelícu
			raf.seek((cod-1)*TAM_FILM);
			// ahora escribo los datos de la película a fichero
			raf.writeInt(cod);
			raf.writeBytes(title + '\n');
			raf.writeInt(year);
			raf.writeBytes(director + '\n');
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void readFile(RandomAccessFile raf)
	{
		try
		{
			// sitúo la marca de posición donde corresponda según el código de la pelícu
			raf.seek((cod-1)*TAM_FILM);
			// ahora leo los datos de la película
			cod=raf.readInt();
			if (cod != 0)
			{
				title=raf.readLine();
				year=raf.readInt();
				director=raf.readLine();
			}
		}
		catch (EOFException e)
		{
			System.out.printf("Esa película no existe\n");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public int getYear() { return year; }
}

public class ejAleatorioFilms
{
	public static void main(String[] args) throws FileNotFoundException,IOException
	{
		Scanner ent=new Scanner(System.in);
		RandomAccessFile raf=new RandomAccessFile("films.dat","rw");
		System.out.printf("GESTIÓN VIDEOTECA\n==========\n\t1. Introducir película\n\t2. Mostrar película\n");
				int opc=ent.nextInt();
				int c; Film f;
				switch (opc)
				{
					case 1: System.out.printf("Introduce código:");
					c=ent.nextInt();
					// evito el problema del buffer del teclado
					ent.nextLine();
					System.out.printf("Introduce título:");
					String t=ent.nextLine();
					System.out.printf("Introduce año:");
					int y=ent.nextInt();
					// problema del buffer del teclado
					ent.nextLine();
					System.out.printf("Introduce director:");
					String d=ent.nextLine();
					f=new Film(c,t,y,d);
					f.writeFile(raf);
					break;

					case 2: System.out.printf("Introduce código:");
						c=ent.nextInt();
						f=new Film(c,"",0,"");
						f.readFile(raf);
						if (f.getYear() != 0)
							f.show();
						break;

					default: if (raf != null) raf.close();
					System.out.printf("Fin del programa");
				};
	}
}
