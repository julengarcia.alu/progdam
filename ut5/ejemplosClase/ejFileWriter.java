/* Programa que pida líneas de texto, hasta acabar
 con una cadena vacía, y las escriba a fichero 
 utilizando la clase FileWriter. */

import java.io.*;

 public class ejFileWriter
 {
 	public static void main(String[] args) {
 		try
 		{
	 		FileWriter fw = new FileWriter("texto.txt",true);
	 		BufferedReader ent = new BufferedReader(new InputStreamReader(System.in));

	 		System.out.println("Línea (vacía para acabar):");
	 		String linea = ent.readLine();
	 		while (!linea.isEmpty())	// mientras la línea tenga contenido
	 		{
	 			fw.write(linea);
	 			System.out.println("Línea (vacía para acabar):");
	 			linea = ent.readLine();
	 			fw.write('\n');	// Añado el cambio de línea
	 		}
	 		if (fw != null)
	 			fw.close();
	 	}
	 	catch (IOException e)
	 	{
	 		System.err.println(e.getMessage());
	 	}
 	}
 }