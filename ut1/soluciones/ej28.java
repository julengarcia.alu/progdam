//28.Escribir un programa que determine los n primeros números de Fibonacci, siendo n un valor introducido por teclado.

public class ej28{
	
	public static void main (String args[])
	{
		
		int n, n1=1, n2=1, n3=2; 
		int cont=1;
		
		System.out.println("Introduce un valor entero positivo: ");
		n=Integer.parseInt(System.console().readLine());
		System.out.println("Los " + n + " primeros términos de Fibonacci son:");
/*		if (n == 1)
			System.out.println("El primer valor de Fibonacci es 1");
		else
			if (n==2)
				System.out.println("Los dos primeros valores de Fibonacci son:\n1\n1");
			else*/
				
		//Mientras que el contador sea menor que n, n3 guardará la suma de n1 y n2 y el valor de n2 pasará a ser n1 y el valor de n3 pasará a ser n3
		while (cont <= n)
		{
			System.out.println(n1);
			n3=n1+n2;	// cada valor es la suma de los 2 anteriores
			n1=n2;
			n2=n3;
			cont++;
		}
	}
}
