// 18. Diseñar un programa que muestre la suma de los números impares comprendidos entre dos valores numéricos enteros y positivos introducidos por teclado.

import java.util.Scanner;

public class ej18Modular
{
    public static int sacar_suma(int a, int b)
    {
        int suma = 0, aux;
        if(a > b)
        {
            aux = a;
            a = b;
            b = aux;
        }
        if ((a % 2) == 0)	// si el primero, el menor, es par
        	a++;	// ahora será el siguiente (impar)
        for(int i = a; i <= b;i+=2)
            //if(i % 2 != 0)
                suma += i;	// suma = suma + i
        return suma;
    }

	public static void main(String args[])
	{
        Scanner teclado = new Scanner(System.in);
        int num1, num2;
        do
        {
            System.out.print("Dime 2 numeros positivos fiera: ");
            
 			num1 = teclado.nextInt();
            num2 = teclado.nextInt();
        }while((num1 < 0) || (num2 < 0));
        
        System.out.println("La suma de los impares es " + sacar_suma(num1,num2));  
	}
}
