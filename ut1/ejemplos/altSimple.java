// Programa que pida un número y lo muestre con signo positivo

public class altSimple
{
	public static void main(String args[])
	{
		System.out.println("Introduce un número:");
		double num = Double.parseDouble(System.console().readLine());
		if (num < 0)
			num = -num;
		System.out.println("El valor en positivo es " + num);
	}
}
