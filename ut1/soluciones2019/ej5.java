// Programa que a partir de un entero introducido desde teclado, muestra todos los enteros, desde 1, menores que él.
public class ej5
{
	public static void main (String args [])
	{
		int num;

		System.out.println("Introduce un entero, por favor:");
		num = Integer.parseInt(System.console().readLine());
		
		do
		{
			num--;
			System.out.println(num);
		} while (num > 1);
	}
}
