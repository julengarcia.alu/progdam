//Algoritmo que lea números desde teclado y finalice cuando se introduzca uno mayor que la suma de los dos anteriores. Escribir en pantalla el numero de valores introducidos y los valores que cumplieron la condición de finalización.

import java.util.Scanner;

public class ej24{
	public static void main (String args[]){

		Scanner entrada = new Scanner (System.in);
		double num = 0, num1 = 0, num2 = 0, suma=0;
		int cont=0, numCiclo=1;

		
		while(true)
		{
			System.out.print("Introduzca un número: ");
			num = entrada.nextDouble();
			cont++;

			if (cont > 2)
				if ( num > suma)
					break;

			if (numCiclo == 1)
			{
				num1 = num;
				numCiclo = 2;
			}
			else if (numCiclo == 2)
			{
				num2 = num;
				numCiclo = 1;
			}
			
			suma = (num1 + num2);						

		}			


		System.out.println("Se han introducido " + cont + " números.");
		System.out.println("Los dos números anteriores fueron " + num1 + " y " + num2 + " y suman " + suma);

	}
}
