/* Realiza un programa que:
 * define la clase Pajaro con un atributo longitudPico, un constructor con un parametro, setter, getter, metodo pia()(que muestre en la pantalla "PIO") y metodo abstracto vuela().
 * define la clase colibri que herede de la anterior e incluya un atributo color de tipo String, 
 con su correspondiente setter y getter, un constructor con 2 parametros desde el cual se llame al constructor de su clase base, 
 un constructor de copia, y redefina o sebreescriba el metodo pia (un colibri hace "PIO, PIO") y lo hace llamando dos veces al metodo pia() de su clase base. 
 Ademas, un colibri vuela a 30km/h (lo muestra por pantalla al volar), y empolla sus huevos (muestra por pantalla que esta empollando sus huevos).
 * crea un objeto colibri y hazlo piar, volar y empollar
 * crea un segundo colibri, copia del anterior.
 * Que cambiaria si los objetos colibri se crearan con una referencia a Pajaro? Explicalo en el codigo fuente.*/
 
abstract class Pajaro{ //define la clase Pajaro
	protected float longitudPico; //con un atributo longitudPico
	public Pajaro(float longitudPico){this.longitudPico=longitudPico;} //un constructor con un parametro
	public void setLongitudPico(float longitudPico){this.longitudPico = longitudPico;} // setter
	public float getLongitudPico(){return longitudPico;} //getter
	public void pia(){System.out.print("PIO");} // metodo pia()(que muestre en la pantalla "PIO")
	public abstract void vuela(); //y metodo abstracto vuela().
}

class Colibri extends Pajaro{  //define la clase colibri que herede de la anterior 
	private String color; //incluya un atributo color de tipo String
	public void setColor(String color){this.color = color;} //con su correspondiente setter
	public String getColor(){return color;} //y getter
	public Colibri(float longitudPico, String color){super(longitudPico);this.color=color;} //un constructor con 2 parametros desde el cual se llame al constructor de su clase base
	public Colibri(Colibri colibri){this(colibri.longitudPico,colibri.color);} //un constructor de copia
	@Override 
	public void pia(){super.pia();System.out.print(", ");super.pia();System.out.println();} //y redefina o sebreescriba el metodo pia (un colibri hace "PIO, PIO") y lo hace llamado dos veces al metodo pia() de su clase base.
	private static final int velocidad=30; //Ademas, un colibri vuela a 30km/h 
	@Override
	public void vuela(){System.out.println("el colibrí vuela a "+velocidad+"km/h");} //(lo muestra por pantalla al volar)
	public void empolla(){System.out.println("el Colibrí está empollando sus huevos");} //y empolla sus huevos (muestra por pantalla que esta empollando sus huevos).
}

public class ej5{
	public static void main(String[] args){
		Colibri c1 = new Colibri(10,"rojo"); //crea un objeto colibri
		c1.pia(); //y hazlo piar
		c1.vuela(); //volar
		c1.empolla(); //y empollar
		Colibri c2 = new Colibri(c1); //crea un segundo colibri, copia del anterior.		
        //¿Que cambiaria si los objetos colibri se crearan con una referencia a Pajaro?//
		Pajaro p1 = new Colibri(20,"amarillo"); //referencia Pajaro = objeto Colibri
		p1.pia(); 
		p1.vuela();
		//Hasta aqui no hay problema, ya que una referencia de Pajaro contiene estos metodos. 
		((Colibri)p1).empolla();
		//En cambio, empolla(), está definido en Colibrí únicamente, así que no es posible ejecutarlo con una referencia de Pajaro.
	}
}
