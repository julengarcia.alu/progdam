/*4. a) Define una clase Vehiculo (ruedas, velocidad y velocidad máxima) que permita detener, acelerar y mover cada vehículo.

Define dos clases derivadas, Bicicleta (marchas) y Motorizado(potencia en CV, caballos de vapor, de tipo double).

Define, a su vez, dos clases derivadas de Motorizado: Motocicleta (tipo) y Automóvil (puertas).

Instancia las 4 clases poniendo a prueba todos sus métodos.

b) Modifica la clase Vehiculo para hacerla abstracta. Incluye en Motorizado un método abstracto getPotenciaReal. La potencia real para motocicletas se expresará en vatios, y para automóviles en kilovatios. (1 Kw=1.36 CV).

c) Modifica la clase Motorizado para hacer getPotenciaReal no abstracta, retornando la potencia en CV.

Crea una referencia a Motorizado y haz que apunte a un objeto Automóvil. ¿Puedes mostrar sus número de puertas? ¿Cómo lo has hecho para no obtener error de compilación? Llama a getPotenciaReal para esa referencia. ¿En qué unidades muestra el valor de potencia? ¿Porqué? ¿Cómo se llama a la relación entre los métodos getPotenciaReal de Motorizado y Automóvil? ¿Podrías hacer un ejemplo de overloading entre ambas clases?

Adapta el programa a estos cambios.

*/
abstract class Vehiculo
{
	protected int ruedas;
	protected double vel;
	protected double velMax;

	public Vehiculo() { ruedas=4; vel=10; velMax=120; }
	public Vehiculo(int r, double v, double vMax) { ruedas=r; vel=v; velMax = vMax; }

	public void acelerar(double v)
	{
		if ((vel+v) >= velMax)
			vel = velMax;
		else
			vel+=v;
	}
	public void detener() { vel=0; }
	public String toString() { return "Vehículo:\n" + "Ruedas: " + ruedas + " Velocidad: " + vel + " Velocidad Máxima: " + velMax; }
}

class Bicicleta extends Vehiculo
{
	protected int marchas;

	public Bicicleta() { marchas=6; }
	public Bicicleta(int r, double v, double vMax, int m) { super(r,v,vMax); marchas=m; }
	public String toString() { return "Bicicleta:\n" + "Ruedas: " + ruedas + " Velocidad: " + vel + " Velocidad Máxima: " + velMax + " Marchas: " + marchas; }	
}

/*abstract*/ class Motorizado extends Vehiculo
{
	//protected double CV;
	protected double cabVapor;

	public Motorizado() { cabVapor=120; }
	public Motorizado(int r, double v, double vMax, double cabVap) { super(r,v,vMax); cabVapor=cabVap; }

	public double getPotenciaReal()
	{
		return cabVapor;
	}
	
	public String toString() { return "Motorizado:\n" + "Ruedas: " + ruedas + " Velocidad: " + vel + " Velocidad Máxima: " + velMax + " Caballos de vapor: " + cabVapor; }
}

class Motocicleta extends Motorizado
{
	protected String tipo;

	public Motocicleta() { tipo = "Motocross"; }
	public Motocicleta(int r, double v, double vMax, double cabVap, String t) { super(r,v,vMax,cabVap); tipo=t; }
	public double getPotenciaReal() { return 1000*cabVapor/1.36; }	
	public String toString() { return "Motocicleta:\n" + "Ruedas: " + ruedas + " Velocidad: " + vel + " Velocidad Máxima: " + velMax + " Caballos de vapor: " + cabVapor + " Tipo: " + tipo; }
}
class Automovil extends Motorizado
{
	protected int puertas;

	public Automovil() { puertas = 4; }
	public Automovil(int r, double v, double vMax, double cabVap, int p) { super(r,v,vMax,cabVap); puertas=p; }
	// sobreescritura
	public double getPotenciaReal() { return cabVapor/1.36; }
	public double getPotenciaReal(char c) // sobrecarga, NO sobreescritura
	{ 
		if (c == 'c")
			return cabValor;
		else	// c == 'k'
			return cabVapor/1.36;
	 }
	public int getPuertas() { return puertas; }
	public String toString() { return "Automóvil:\n" + "Ruedas: " + ruedas + " Velocidad: " + vel + " Velocidad Máxima: " + velMax + " Caballos de vapor: " + cabVapor + " Puertas: " + puertas; }
}
public class ej4c
{
	public static void main(String[] args)
	{
		Bicicleta b1 = new Bicicleta(2,12,26,3);
		Motocicleta mt = new Motocicleta();
		Automovil aut = new Automovil(6,80,200,130,8);

		
		System.out.println(b1);
		System.out.println(mt);
		System.out.println(aut);
	
		mt.acelerar(8); //la motocicleta aumenta su velocidad sin superar la velocidad máxima
		aut.acelerar(200); //el automóvil llega a su velocidad máxima, sin poder superarla
		b1.detener(); //la bicicleta se detiene
		
		System.out.println();
		System.out.println(b1);
		System.out.println(mt);
		System.out.println(aut);

		System.out.println();
		System.out.println("La potencia real de la motocicleta es: " + mt.getPotenciaReal() + " vatios");
		System.out.println("La potencia real del automovil es: " + aut.getPotenciaReal() + " Kw");
		
		Motorizado mtz = new Motorizado(3,20,60,10);
		System.out.println("La potencia del objeto motorizado es " + mtz.getPotenciaReal());

		mtz = aut;	// tipo estático es Motorizado(base) y el dinámico es Automóvil (derivada)
		System.out.println("Tiene " + ((Automovil)mtz).getPuertas() + "puertas");
		System.out.println("La potencia es " + mtz.getPotenciaReal());
		
		
		
	}
}
