// Ejemplo de parámetro pasado por valor (la función trabaja con una copia del dato, no el original)
import java.util.Scanner;

public class pasoPorValor
{
	public static void main(String[] args) {
		double n;
		Scanner ent = new Scanner(System.in);
		
		System.out.println("Introduce un número:");
		n = ent.nextDouble();
		doble(n);
		System.out.println("El valor duplicado es " + n);
				
		System.exit(0);
	}
	
	public static void doble(double x)
	{
		x = 2*x;
		//System.out.println("El valor duplicado en la función es " + x);
	}
}
