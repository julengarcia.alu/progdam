class rectangulo
{
	private int ancho;
	private int alto;
	// 3 constructores "clasicos"
	public rectangulo(int an, int al){
		ancho = an;
		alto = al;
	}
	public rectangulo(){ ancho=alto=0;	}
	public rectangulo(int dato){ ancho=alto=dato; }
	// nuevo tipo de constructor: el constructor de copia
	public rectangulo(rectangulo r)
	{
		this.ancho=r.ancho;
		this.alto=r.alto;
	}
	public int getAncho(){return this.ancho;}
	public int getAlto(){return this.alto;}
	public rectangulo incrementarAncho(){
		ancho++;
		return this;
	}
	public rectangulo incrementarAlto(){
		alto++;
		return this;
	}
	public void finalize()
	{
		System.out.println("Adiósss");
	}	
}

public class finalize
{
	public static void main(String[] args)
	{
		rectangulo r;
		for (int i=0;i<10;i++)
		{
			r=new rectangulo(5,5);
		}
		if (true)
		{
			rectangulo r1=new rectangulo(5,7);
			// creo el segundo rectangulo con el constructor de copia
			rectangulo r2=new rectangulo(r1);
			rectangulo r3=new rectangulo(1,2);
		}
		
		
		/*r1=r2;
		r3=r2;*/
		System.runFinalization();
		System.gc();
		//System.out.println("Alto:" + r2.getAlto());
		//System.out.println("Ancho:" + r2.getAncho());
		
		System.exit(0);
	}
}
