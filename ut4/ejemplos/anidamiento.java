// ejemplo de clase anidada no estática

class A
{
	private B b;
	public A() { b= new B();/*b.num=0;*/}
	public void setB(B b) { this.b=b;}
	class B
	{
		private /* static*/ int num;
	}
}

public class anidamiento
{
	public static void main(String args [])
	{
		A a=new A();
		//B b = new B();
		//A.B ab = new A.B();
		a.setB(a.new B());
		A.B ab = a.new B();	// necesito la instancia de la clase exterior
	}
}
