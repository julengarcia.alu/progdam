// En este programa clonamos un objeto dibujo para comprobar si original y copia son, o no, completamente independientes

public class programa
{
	public static void main(String[] args) {
		dibujo d1 = new dibujo();	// num=1, rectángulo y círculo unitarios
		dibujo d2 = /*(dibujo)*/ d1.clone();
		dibujo d3 = new dibujo(d1);	// utilizando el Constructor de copia
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
		d2.setNum(2);
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
		// cambio el ancho del rectángulo en el primer dibujo a 10
		d1.getRectangulo().setAncho(10);
		//d1.setAncho(10);
		d3.getCirculo().setRadio(8);
		System.out.println("Dibujos después de cambiar el ancho del rectángulo del primer dibujo:");
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);	
		System.exit(0);
	}
}
